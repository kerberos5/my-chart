# Test Helm Chart Repos

### Install Helm by dnf
``` bash
$ sudo dnf install helm -y
$ helm version
```
### Install Helm by repos
``` bash
$ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
$ sudo chmod +x get_helm.sh
$ ./get_helm.sh
$ helm version
```
### Install Helm from archive
``` bash
$ tar -zxvf helm-vxxx-xxxx-xxxx.tar.gz
$ sudo mv linux-amd64/helm /usr/local/bin/helm
$ helm version
```
### bash-completion
``` bash
$ helm completion bash > helm-kubernetes.sh
$ sudo cp helm-kubernetes.sh /etc/bash_completion.d/